package com.example.prana.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.example.prana.R
import com.example.prana.databinding.ActivityDashboardBinding
import com.example.prana.fragment.BreatheFragment
import com.example.prana.fragment.HomeFragment
import com.example.prana.fragment.ProfileFragment

class DashboardActivity : AppCompatActivity(){

    private var homeF = HomeFragment()
    private var breatheF = BreatheFragment()
    private var profileF = ProfileFragment()

    private lateinit var binding: ActivityDashboardBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDashboardBinding.inflate(layoutInflater)
        setContentView(binding.root)
        switchFragment(homeF)
        setupView()
    }

    override fun onResume() {
        super.onResume()
        selectNavigationItem(binding.homeBNV.selectedItemId)

    }

    private fun setupView() {
        binding.homeBNV.setOnNavigationItemSelectedListener { menuItem ->
            selectNavigationItem(menuItem.itemId)
        }
    }

    private fun selectNavigationItem(itemId: Int): Boolean{
        return when(itemId){
        R.id.homeTB ->{
            switchFragment(homeF)
            true
        }
        R.id.breatheTB ->{
            switchFragment(breatheF)
            true
        }
        R.id.profileTB ->{
            switchFragment(profileF)
            true
        }
        else -> {
            switchFragment(homeF)
            false
        }
    }
    }

    private fun switchFragment(fragment: Fragment) {
        val fragmentManager: FragmentManager = supportFragmentManager
        val transaction: FragmentTransaction = fragmentManager.beginTransaction()
        transaction.replace(binding.dashboardFL.id, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }
}