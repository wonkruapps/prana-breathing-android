package com.example.prana.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.prana.R
import com.example.prana.adapter.BreathHealthAdapter
import com.example.prana.databinding.FragmentStatsBinding
import com.example.prana.model.BreathModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class StatsFragment : Fragment() {

    private lateinit var  binding: FragmentStatsBinding


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
       binding = FragmentStatsBinding.inflate(layoutInflater)
       return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        CoroutineScope(Dispatchers.IO).launch {
            setupView()
            setAdapter()
        }
    }

    private fun setupView() : ArrayList<BreathModel> {
        val breathList = ArrayList<BreathModel>()
        breathList.add(BreathModel("Breath hold","Current max breath hold","30s"))
        breathList.add(BreathModel("Maximum exhale","Current max exhale","17s"))
        breathList.add(BreathModel("Balance","breath hold","35s"))
        breathList.add(BreathModel("Energize","max exhale","35s"))
        return breathList
    }

    private fun setAdapter() {
        binding.testBreathHealthRV.layoutManager = LinearLayoutManager(requireActivity(),LinearLayoutManager.VERTICAL,false)
        val adapter = BreathHealthAdapter(setupView())
        binding.testBreathHealthRV.adapter = adapter
    }

}