package com.example.prana.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.prana.R
import com.example.prana.databinding.FragmentBreatheBinding

class BreatheFragment : Fragment() {
    private lateinit var binding: FragmentBreatheBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
       binding = FragmentBreatheBinding.inflate(layoutInflater)
        return binding.root
    }

}