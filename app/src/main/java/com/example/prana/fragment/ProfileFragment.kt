package com.example.prana.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.example.prana.R
import com.example.prana.databinding.FragmentProfileBinding
import com.google.android.material.tabs.TabLayout

class ProfileFragment : Fragment() {

    private lateinit var binding: FragmentProfileBinding

    private val progressF = ProgressFragment()
    private val statsF = StatsFragment()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding =FragmentProfileBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        switchFragment(statsF)
        setupListeners()
    }

    private fun setupListeners() {
        binding.profileTLT.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener{
            override fun onTabSelected(tab: TabLayout.Tab?) {
                when(tab?.position){
                    0->{switchFragment(statsF)}
                    1->{switchFragment(progressF)}
                    else -> switchFragment(statsF)
                }
            }
            override fun onTabUnselected(tab: TabLayout.Tab?) {}
            override fun onTabReselected(tab: TabLayout.Tab?) {}
        })
    }

    private fun switchFragment(fragment: Fragment) {
        val fragmentManager = childFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(binding.ProfileFL.id, fragment)
        fragmentTransaction.commit()
    }

}