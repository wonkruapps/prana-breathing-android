package com.example.prana.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.example.prana.R
import com.example.prana.model.BreathModel

class BreathHealthAdapter(private val breathList : ArrayList<BreathModel>) : RecyclerView.Adapter<BreathHealthAdapter.BreathHealthVH>() {
    class BreathHealthVH(v: View) : RecyclerView.ViewHolder(v) {
       val iconLayoutCL : ConstraintLayout = v.findViewById(R.id.iconLayoutCL)
       val breatheHealthIV : ImageView = v.findViewById(R.id.breatheHealthIV)
       val breatheHealthTitleTV : TextView = v.findViewById(R.id.breatheHealthTitleTV)
       val breatheHealthSecTV : TextView = v.findViewById(R.id.breatheHealthSecTV)
       val breatheHealthSubTitleTV : TextView = v.findViewById(R.id.breatheHealthSubTitleTV)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BreathHealthVH {
        return  BreathHealthVH(LayoutInflater.from(parent.context).inflate(R.layout.breath_health_item,parent,false))
    }

    override fun getItemCount(): Int {
        return breathList.size
    }

    override fun onBindViewHolder(holder: BreathHealthVH, position: Int) {
        val breath = breathList[position]
        holder.breatheHealthTitleTV.text = breath.title
        holder.breatheHealthSubTitleTV.text = breath.subTitle
        holder.breatheHealthSecTV.text = breath.sec
    }
}