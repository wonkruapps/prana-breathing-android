package com.example.prana.model

import java.io.Serializable

data class BreathModel(
    val title : String? = null,
    val subTitle : String? = null,
    val sec : String? = null,
): Serializable
